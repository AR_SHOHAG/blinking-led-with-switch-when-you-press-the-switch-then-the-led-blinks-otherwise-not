void setup()
{
    pinMode(13, OUTPUT);
    pinMode(7, INPUT);
}

int val;

void loop()
{
    val = digitalRead(7);
    if(val == LOW)
        digitalWrite(13, HIGH);
    else
        digitalWrite(13, LOW);
}
